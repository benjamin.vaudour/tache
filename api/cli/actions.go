package cli

import (
	"fmt"
	"strings"

	. "framagit.org/benjamin.vaudour/dctime"
	"framagit.org/benjamin.vaudour/shell/console"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

const (
	ahelp      = "help"
	ashell     = "shell"
	aexit      = "exit"
	aview      = "view"
	asearch    = "search"
	aclose     = "clo"
	aopen      = "open"
	adelete    = "del"
	aadd       = "add"
	amodify    = "mod"
	aduplicate = "dup"
)

var actions = []string{
	ahelp,
	aview,
	asearch,
	aadd,
	amodify,
	aduplicate,
	aclose,
	aopen,
	adelete,
	ashell,
	aexit,
}

var maction = map[string]*action{
	ahelp: &action{
		name: ahelp,
		help: "Affiche cette aide",
	},
	ashell: &action{
		name:     ashell,
		help:     "passe en mode interactif",
		notshell: true,
	},
	aexit: &action{
		name:    aexit,
		help:    "sort du mode interactif",
		notstat: true,
	},
	aview: &action{
		name:    aview,
		help:    "Affiche une(des) tâche(s)",
		dneeded: true,
		ineeded: true,
		ifac:    true,
	},
	asearch: &action{
		name:    asearch,
		help:    "Affiche une(des) tâche(s) filtrée(s) selon des critères",
		dneeded: true,
		option:  soption,
	},
	aclose: &action{
		name:    aclose,
		help:    "Clôture une(des) tâche(s)",
		dneeded: true,
		ineeded: true,
	},
	aopen: &action{
		name:    aopen,
		help:    "Réouvre une(des) tâche(s)",
		dneeded: true,
		ineeded: true,
	},
	adelete: &action{
		name:    adelete,
		help:    "Supprime une(des) tâche(s)",
		dneeded: true,
		ineeded: true,
	},
	aadd: &action{
		name:    aadd,
		help:    "Ajoute une tâche",
		dneeded: true,
		option:  moption,
	},
	amodify: &action{
		name:    amodify,
		help:    "Modifie une(des) tâche(s)",
		dneeded: true,
		ineeded: true,
		option:  moption,
	},
	aduplicate: &action{
		name:    aduplicate,
		help:    "Duplique une(des) tâche(s)",
		dneeded: true,
		ineeded: true,
		option:  moption,
		ofac:    true,
	},
}

func _mod(t *tache.Tache, args *arguments, r bool) {
	b, e := t.Begin, t.End
	var bm, em, rm bool
	for i, o := range args.options {
		switch o.name {
		case "b", "+b", "-b", "--b":
			bm = true
		case "e", "+e", "-e", "--e":
			em = true
		case "r", "--r":
			rm = true
		}
		o.exec(t, args.args[i])
	}
	if !em && bm && b != ClockNil && e != ClockNil {
		d := t.Begin.Diff(b)
		if d == DurationNil {
			t.End = ClockNil
		} else {
			t.End, _ = e.Add(d)
		}
	}
	if !r && !rm {
		t.Recurrence = DurationNil
	}
}

func WordCompleter(line string, pos int) (head string, completions []string, tail string) {
	head, tail = line[:pos], line[pos:]
	for n, a := range maction {
		if !a.notshell && strings.HasPrefix(n, head) {
			completions = append(completions, n)
		}
	}
	if len(completions) > 0 {
		head = ""
		tail = " " + tail
	}
	return
}

var aexec = map[string]func(*App, *arguments){
	ahelp: func(app *App, args *arguments) {
		helpUsage(app.name)
		helpActions(app.interactive)
		helpOptions()
	},
	ashell: func(app *App, args *arguments) { app.interactive = true },
	aexit:  func(app *App, args *arguments) { app.interactive = false },
	aview: func(app *App, args *arguments) {
		var req tache.List
		db := app.db()
		if len(args.ids) == 0 {
			req = db.FindAll()
		} else {
			req = db.Find(args.ids...)
		}
		tsks := req.Filter(func(t *tache.Tache) bool { return !t.Deleted })
		app.view.printAllTasks(tsks, app.view.vfields)
	},
	asearch: func(app *App, args *arguments) {
		query := make([]tache.FilterFunc, 0, len(args.options))
		not := false
		for i, o := range args.options {
			if o.name == onot {
				not = !not
			} else {
				f := o.filter(args.args[i])
				if not {
					f = tache.FilterNot(f)
				}
				query, not = append(query, f), false
			}
		}
		db := app.db()
		tsks := db.Search(query...).Sort(tache.LessById)
		app.view.printAllTasks(tsks, app.view.vfields)
	},
	aclose: func(app *App, args *arguments) {
		c, done, a := 0, make(map[int]bool), "close"
		db := app.db()
		for _, id := range args.ids {
			if done[id] {
				continue
			}
			done[id] = true
			if t, pass := db.Close(id); pass {
				c++
				app.view.printAction(t, a)
			}
		}
		config := app.conf()
		db.Clean(config.GetInt("main.recurrences"), tache.LessByAll)
		fmt.Println()
		app.view.printCount(c, a)
	},
	aopen: func(app *App, args *arguments) {
		c, done, a := 0, make(map[int]bool), "ouverte"
		db := app.db()
		for _, id := range args.ids {
			if done[id] {
				continue
			}
			done[id] = true
			if t, pass := db.Open(id); pass {
				c++
				app.view.printAction(t, a)
			}
		}
		config := app.conf()
		db.Clean(config.GetInt("main.recurrences"), tache.LessByAll)
		fmt.Println()
		app.view.printCount(c, a)
	},
	adelete: func(app *App, args *arguments) {
		c, done, a := 0, make(map[int]bool), "supprimée"
		db := app.db()
		for _, id := range args.ids {
			if done[id] {
				continue
			}
			if tsks := db.Find(id); len(tsks) > 0 {
				t, r := tsks[0], false
				if t.IsRecurrent() {
					r = console.QuestionBool(fmt.Sprintf(deleteAllQuestion, t.Id, t.Description), false)
				}
				if del, pass := db.Del(id, r); pass {
					for _, tt := range del {
						c++
						app.view.printAction(tt, a)
						done[tt.Id] = true
					}
				}
			}
		}
		config := app.conf()
		db.Clean(config.GetInt("main.recurrences"), tache.LessByAll)
		fmt.Println()
		app.view.printCount(c, a)
	},
	aadd: func(app *App, args *arguments) {
		t, a := tache.New(), "ajoutée"
		for i, o := range args.options {
			o.exec(t, args.args[i])
		}
		config, db := app.conf(), app.db()
		recs := config.GetInt("main.recurrences")
		var tsks tache.List
		if db.Add(t) {
			if t.IsRecurrent() {
				tsks = db.AddMissingTasks(t.Rid, recs)
			} else {
				tsks.Append(t)
			}
			db.Clean(recs, tache.LessByAll)
			for _, t := range tsks {
				app.view.printAction(t, a)
			}
			fmt.Println()
			app.view.printCount(len(tsks), a)
		}
	},
	amodify: func(app *App, args *arguments) {
		var la, lm, ld tache.List
		aa, am, ad := "ajoutée", "modifiée", "supprimée"
		done := make(map[int]bool)
		config, db := app.conf(), app.db()
		recs := config.GetInt("main.recurrences")
		for _, id := range args.ids {
			if done[id] {
				continue
			}
			done[id] = true
			tsks := db.Find(id)
			if len(tsks) == 0 {
				continue
			}
			t := tsks[0]
			if t.Deleted {
				continue
			}
			r := false
			if t.IsRecurrent() {
				r = console.QuestionBool(fmt.Sprintf(modifyAllQuestion, t.Id, t.Description), false)
			}
			_mod(t, args, r)
			if del, add, mod, pass := db.Modify(t, r); pass {
				for _, tt := range del {
					done[tt.Id] = true
				}
				ld.Append(del...)
				if add == nil {
					lm.Append(mod)
				} else if add.IsRecurrent() {
					for _, tt := range db.AddMissingTasks(add.Rid, recs) {
						la.Append(tt)
						done[tt.Id] = true
					}
				} else {
					la.Append(add)
					done[add.Id] = true
				}
			}
		}
		db.Clean(recs, tache.LessByAll)
		for _, t := range lm {
			app.view.printAction(t, am)
		}
		for _, t := range ld {
			app.view.printAction(t, ad)
		}
		for _, t := range la {
			app.view.printAction(t, aa)
		}
		fmt.Println()
		app.view.printCount(len(lm), am)
		if len(ld) > 0 {
			app.view.printCount(len(ld), ad)
		}
		if len(la) > 0 {
			app.view.printCount(len(la), aa)
		}
	},
	aduplicate: func(app *App, args *arguments) {
		c, a := 0, "dupliquée"
		done := make(map[int]bool)
		config, db := app.conf(), app.db()
		for _, id := range args.ids {
			if done[id] {
				continue
			}
			done[id] = true
			tsks := db.Find(id)
			if len(tsks) == 0 {
				continue
			}
			t := tsks[0]
			if t.Deleted {
				continue
			}
			tc := t.Clone()
			_mod(tc, args, false)
			if db.Add(tc) {
				app.view.printAction(t, a)
				c++
				done[t.Id] = true
			}
		}
		db.Clean(config.GetInt("main.recurrences"), tache.LessByAll)
		app.view.printCount(c, a)
	},
}
