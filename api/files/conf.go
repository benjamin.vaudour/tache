package files

import (
	"path"

	"framagit.org/benjamin.vaudour/conf"
	"framagit.org/benjamin.vaudour/tache/api/database"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

const Template = `
[main]
filepath    =
filename    = tache.json
recurrences = 3
colsep      = space
prompt      = tache >

[color]
nocolor = 0
counter = yellow
header  = underline,bold
bg1     =
bg2     = black
forever = italic
high    = l_red,bold
medium  = l_yellow
low     = l_green
past    = red
today   = l_red
current = blink
future  =
eow     = l_yellow
7days   = l_green
1month  = l_cyan
deleted = crossed
prompt  = l_green

[column]
id.length          = 3
id.align           = r
id.label           = ID
rid.length         = 5
rid.align          = r
rid.label          = RID
category.length    = 16
category.align     = l
category.label     = Catégorie
priority.length    = 3
priority.align     = c
priority.label     = Pri
date.length        = 10
date.align         = c
date.label         = Date
begin.length       = 5
begin.align        = c
begin.label        = Début
end.length         = 5
end.align          = c
end.label          = Fin
recurrence.length  = 3
recurrence.align   = c
recurrence.label   = Réc
description.length = 35
description.align  = l
description.label  = Description
deleted.length     = 3
deleted.align      = c
deleted.label      = Sup

order = id,category,priority,date,begin,end,recurrence,description
`

var loaders = map[string]database.Loader{
	"json": database.NewJSONLoader(),
}

type Files struct {
	Conf       *conf.ConfigurationFile
	Db         *database.DatabaseFile
	IsDbLoaded bool
}

func NewFiles(confPath string) *Files {
	cf := conf.NewFile(Template, confPath)
	cf.Load()
	dbDir := cf.Configuration.Get("main.filepath")
	dbFile := cf.Configuration.Get("main.filename")
	if dbDir == "" {
		dbDir = path.Dir(confPath)
	}
	loader, ok := loaders[path.Ext(dbFile)]
	if !ok {
		loader = database.NewJSONLoader()
	}
	df := database.NewFile(loader, path.Join(dbDir, dbFile))
	return &Files{
		Conf: cf,
		Db:   df,
	}
}

func (f *Files) SaveConfig() error {
	return f.Conf.Save()
}

func (f *Files) LoadDatabase() error {
	f.IsDbLoaded = true
	return f.Db.Load()
}

func (f *Files) SaveDatabase() error {
	return f.Db.Save()
}

func (f *Files) Configuration() *conf.Configuration {
	return f.Conf.Configuration
}

func (f *Files) Database() *tache.Database {
	if !f.IsDbLoaded {
		f.LoadDatabase()
	}
	return f.Db.Database
}
