package tache

type Priority uint8

const (
	NoPriority Priority = iota
	Low
	Medium
	High
)

var (
	p2s = map[Priority]string{
		NoPriority: "-",
		Low:        "L",
		Medium:     "M",
		High:       "H",
	}

	s2p = func() map[string]Priority {
		m := make(map[string]Priority)
		for p, s := range p2s {
			m[s] = p
		}
		return m
	}()
)

func (p Priority) IsNil() bool {
	if p == NoPriority {
		return true
	}
	_, exist := p2s[p]
	return !exist
}

func (p Priority) String() string {
	if s, ok := p2s[p]; ok {
		return s
	}
	return p2s[NoPriority]
}

func (e1 Priority) Compare(e2 Priority) int {
	if e1 < e2 {
		return -1
	} else if e1 > e2 {
		return 1
	}
	return 0
}

func ParsePriority(in string) (out Priority, ok bool) {
	if out, ok = s2p[in]; !ok {
		out = NoPriority
	}
	return
}
