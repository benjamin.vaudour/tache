package cli

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"framagit.org/benjamin.vaudour/shell/console"
)

func (arg *arguments) parse(args []string) (a *action, msg *console.Error) {
	e, ok := "", false
	if len(args) == 0 {
		a = maction[aview]
		return
	}
	e, args = args[0], args[1:]
	if a, ok = maction[e]; !ok {
		msg = console.NewErrorf(ewarning, actionError, e, invalidAction)
		return
	}
	if a.ineeded {
		if len(args) == 0 {
			if !a.ifac {
				msg = console.NewErrorf(ewarning, actionError, a.name, idNeeded)
			}
			return
		}
		e, args = args[0], args[1:]
		if arg.ids, ok = parseId(e); !ok {
			msg = console.NewErrorf(ewarning, actionError, a.name, fmt.Sprintf(idNotParsed, e))
			return
		}
	}
	if len(args) == 0 {
		if len(a.option) > 0 && !a.ofac {
			msg = console.NewErrorf(ewarning, actionError, optionsNeeded)
			return
		}
	}
	for len(args) > 0 {
		e, args = args[0], args[1:]
		var o *option
		var v interface{}
		if o, ok = a.option[e]; !ok {
			msg = console.NewErrorf(ewarning, actionError, a.name, fmt.Sprintf(invalidOption, e))
			return
		}
		if len(o.arg) > 0 {
			if len(args) == 0 {
				msg = console.NewErrorf(ewarning, actionError, a.name, fmt.Sprintf(optionNeedArg, e))
				return
			}
			e, args = args[0], args[1:]
			if v, ok = o.valid(e); !ok {
				msg = console.NewErrorf(ewarning, actionError, a.name, fmt.Sprintf(invalidArg, e, o.name))
				return
			}
		}
		arg.options, arg.args = append(arg.options, o), append(arg.args, v)
	}
	return
}

func parseId(e string) (ids []int, ok bool) {
	if ok, _ = regexp.MatchString(`^(\d+(-\d+)?(,\d+(-\d+)?)*|all)$`, e); ok && e != "all" {
		rgs := strings.Split(e, ",")
		for _, s := range rgs {
			rg := strings.Split(s, "-")
			if len(rg) == 1 {
				id, _ := strconv.Atoi(rg[0])
				ids = append(ids, id)
			} else {
				i0, _ := strconv.Atoi(rg[0])
				i1, _ := strconv.Atoi(rg[1])
				sub := i0 > i1
				for {
					ids = append(ids, i0)
					if i0 == i1 {
						break
					} else if sub {
						i0--
					} else {
						i0++
					}
				}
			}
		}
	}
	return
}
