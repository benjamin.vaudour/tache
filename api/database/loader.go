package database

import (
	"io"
	"os"

	"framagit.org/benjamin.vaudour/tache/api/tache"
)

type EncodeFunc func(*tache.Database, io.Writer) error

type DecodeFunc func(*tache.Database, io.Reader) error

type Loader struct {
	Encode EncodeFunc
	Decode DecodeFunc
}

func NewLoader(enc EncodeFunc, dec DecodeFunc) Loader {
	return Loader{
		Encode: enc,
		Decode: dec,
	}
}

type DatabaseFile struct {
	Loader
	Database *tache.Database
	Path     string
}

func NewFile(l Loader, filepath string) *DatabaseFile {
	return &DatabaseFile{
		Database: tache.NewDatabase(),
		Loader:   l,
		Path:     filepath,
	}
}

func (df *DatabaseFile) Load() error {
	f, err := os.Open(df.Path)
	if err != nil {
		return err
	}
	defer f.Close()
	return df.Decode(df.Database, f)
}

func (df *DatabaseFile) Save() error {
	f, err := os.Create(df.Path)
	if err != nil {
		return err
	}
	defer f.Close()
	return df.Encode(df.Database, f)
}
