package cli

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"framagit.org/benjamin.vaudour/conf"
	. "framagit.org/benjamin.vaudour/dctime"
	"framagit.org/benjamin.vaudour/shell/console"
	"framagit.org/benjamin.vaudour/strutil/align"
	"framagit.org/benjamin.vaudour/strutil/style"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

func (f *field) format(s string) string      { return align.Align(s, f.align, f.size) }
func (f *field) title() string               { return f.format(f.name) }
func (f *field) value(t *tache.Tache) string { return f.format(f.str(t)) }

var ffields = map[string]func(*tache.Tache) string{
	"id":          func(t *tache.Tache) string { return strconv.Itoa(t.Id) },
	"rid":         func(t *tache.Tache) string { return strconv.Itoa(t.Rid) },
	"category":    func(t *tache.Tache) string { return t.Category },
	"priority":    func(t *tache.Tache) string { return t.Priority.String() },
	"date":        func(t *tache.Tache) string { return t.Date.String() },
	"begin":       func(t *tache.Tache) string { return t.Begin.String() },
	"end":         func(t *tache.Tache) string { return t.End.String() },
	"recurrence":  func(t *tache.Tache) string { return t.Recurrence.String() },
	"description": func(t *tache.Tache) string { return t.Description },
	"deleted": func(t *tache.Tache) string {
		if t.Deleted {
			return "X"
		}
		return ""
	},
}

var suppressField = &field{
	name:  "S",
	size:  1,
	align: align.LEFT,
	str: func(t *tache.Tache) string {
		if t.Deleted {
			return "X"
		}
		return " "
	},
}

func newView(config *conf.Configuration) *view {
	out := new(view)

	//Init prompt
	out.prompt = config.Get("main.prompt") + " "

	//Init col sep
	out.sep = config.Get("main.colsep")
	if out.sep == "space" {
		out.sep = " "
	}

	//Init styles
	out.noColor = config.GetBool("color.nocolor")
	out.headerStyle = config.GetSlice("color.header")
	out.counterStyle = config.GetSlice("color.counter")
	out.bgStyle = []string{config.Get("color.bg1"), config.Get("color.bg2")}
	hstyle := []string{
		"forever",
		"high",
		"medium",
		"low",
		"past",
		"today",
		"current",
		"future",
		"eow",
		"7days",
		"1month",
		"deleted",
	}
	out.taskStyle = make(map[string][]string)
	for _, k := range hstyle {
		out.taskStyle[k] = config.GetSlice("color." + k)
	}

	// Init vfields
	columns := config.GetSlice("column.order")
	out.vfields = make([]*field, len(columns))
	for i, c := range columns {
		fn := ffields[c]
		if fn == nil {
			continue
		}
		k := "column." + c + "."
		f := &field{
			name:  config.Get(k + "label"),
			size:  int(config.GetInt(k + "length")),
			align: align.LEFT,
			str:   fn,
		}
		a := config.Get(k + "align")
		if len(a) > 0 {
			switch a[0] {
			case 'R' | 'r':
				f.align = align.RIGHT
			case 'C' | 'c':
				f.align = align.CENTER
			}
		}
		out.vfields[i] = f
	}

	//Init sfields
	l := len(out.vfields)
	out.sfields = make([]*field, l+1)
	copy(out.sfields[:l], out.vfields)
	out.sfields[l] = suppressField

	return out
}

func (v *view) printHeader(columns []*field) {
	args := make([]string, len(columns))
	for i, f := range columns {
		args[i] = f.title()
	}
	f := style.New(v.headerStyle...)
	if v.noColor {
		f.RemoveBg().RemoveFg()
	}
	fmt.Println(f.Format(strings.Join(args, v.sep)))
}

func (v *view) printTask(t *tache.Tache, columns []*field, f *style.Formatter) {
	args := make([]string, len(columns))
	for i, f := range columns {
		args[i] = f.value(t)
	}
	fmt.Println(f.Format(strings.Join(args, v.sep)))
}

func (v *view) printCount(c int, a string) {
	count := fmt.Sprintf(countNotice, c, a)
	f := style.New(v.counterStyle...)
	if v.noColor {
		f.RemoveBg().RemoveBg()
	}
	fmt.Println(f.Format(count))
}

func (v *view) printAction(t *tache.Tache, a string) {
	fmt.Println(fmt.Sprintf(actionNotice, t.Id, t.Description, a))
}

func (v *view) printAllTasks(tsks tache.List, columns []*field) {
	c := len(tsks)
	if c > 0 {
		v.printHeader(columns)
		d0 := DateNow()
		dw, dm := d0.Eow(), d0.Eom()
		d7, _ := d0.Add(NewDuration(7, Day))
		for i, t := range tsks {
			col := style.New()
			col.AddBg(v.bgStyle[i%2])
			if t.Deleted {
				col.Add(v.taskStyle["deleted"]...)
			}
			switch t.Priority {
			case tache.High:
				col.Add(v.taskStyle["high"]...)
			case tache.Medium:
				col.Add(v.taskStyle["medium"]...)
			case tache.Low:
				col.Add(v.taskStyle["low"]...)
			}
			switch {
			case t.IsForever():
				col.Add(v.taskStyle["forever"]...)
			case t.IsPast():
				col.Add(v.taskStyle["past"]...)
			case t.IsToday():
				col.Add(v.taskStyle["today"]...)
				if t.IsNow() {
					col.Add(v.taskStyle["current"]...)
				}
			default:
				col.Add(v.taskStyle["future"]...)
				switch {
				case !t.IsDateAfter(dw):
					col.Add(v.taskStyle["eow"]...)
				case !t.IsDateAfter(d7):
					col.Add(v.taskStyle["7days"]...)
				case !t.IsDateAfter(dm):
					col.Add(v.taskStyle["1month"]...)
				}
			}
			if v.noColor {
				col.RemoveBg()
				col.RemoveFg()
			}
			v.printTask(t, columns, col)
		}
	}
	fmt.Println()
	v.printCount(c, "affichée")
}

func (v *view) printMessage(m *console.Error) (ret int) {
	f := style.New("bold")
	c := m.Code()
	switch c {
	case ecritical:
		ret = c
		f.AddFg("black").AddBg("red")
	case eerror:
		ret = c
		f.AddFg("l_red").AddBg("black")
	case ewarning:
		f.AddFg("l_yellow").AddBg("black")
	}
	fmt.Fprintln(os.Stderr, f.Format(m.Error()))
	return
}
