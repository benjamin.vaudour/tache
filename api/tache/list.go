package tache

import (
	"sort"

	"framagit.org/benjamin.vaudour/collection/v2"
)

type List []*Tache

type LessFunc func(*Tache, *Tache) bool

type FilterFunc func(*Tache) bool

func (l *List) Append(tasks ...*Tache) List {
	*l = append(*l, tasks...)
	return *l
}

func (l *List) Remove(idx int) List {
	s := len(*l)
	if idx >= 0 && idx < s {
		c := make(List, s-1)
		copy(c[:idx], (*l)[:idx])
		copy(c[idx:], (*l)[idx+1:])
		*l = c
	}
	return *l
}

func (l *List) Delete(idxs ...int) List {
	var c List
	m := collection.SliceToSet(idxs)
	for i, t := range *l {
		if !m[i] {
			c.Append(t)
		}
	}
	*l = c
	return c
}

func (l List) Clone() List {
	c := make(List, len(l))
	for i, t := range l {
		c[i] = t.Clone()
	}
	return c
}

func (l List) Sort(f LessFunc) List {
	sort.Slice(l, func(i, j int) bool { return f(l[i], l[j]) })
	return l
}

func (l List) Filter(f FilterFunc) List {
	var out List
	for _, t := range l {
		if f(t) {
			out.Append(t)
		}
	}
	return out
}

func (l List) FilterNot(f FilterFunc) List { return l.Filter(FilterNot(f)) }

func (l List) Contains(f FilterFunc) bool {
	for _, t := range l {
		if f(t) {
			return true
		}
	}
	return false
}

func (l List) ContainsId(id int) bool { return l.Contains(FilterById(id)) }

func (l List) ContainsRid(rid int) bool { return l.Contains(FilterByRid(rid)) }

func (l List) Count(f FilterFunc) int {
	c := 0
	for _, t := range l {
		if f(t) {
			c++
		}
	}
	return c
}

func (l List) IndexesOf(f FilterFunc) (idxs []int) {
	for i, t := range l {
		if f(t) {
			idxs = append(idxs, i)
		}
	}
	return idxs
}

func (l List) IndexOf(f FilterFunc) (idx int) {
	for i, t := range l {
		if f(t) {
			return i
		}
	}
	return -1
}

func (l List) Find(f FilterFunc) (t *Tache, ok bool) {
	idx := l.IndexOf(f)
	if ok = idx >= 0; ok {
		t = l[idx]
	}
	return
}

func (l List) Split(f FilterFunc) (l1, l2 List) {
	for _, t := range l {
		if f(t) {
			l1.Append(t)
		} else {
			l2.Append(t)
		}
	}
	return
}

func LessById(t1, t2 *Tache) bool { return t1.Id < t2.Id }

func LessByRid(t1, t2 *Tache) bool { return t1.Rid < t2.Rid }

func LessByAll(t1, t2 *Tache) bool {
	d1, d2, p1, p2 := t1.Date, t2.Date, t1.Priority, t2.Priority
	b1, e1 := t1.Clock()
	b2, e2 := t2.Clock()
	if c := d1.Compare(d2); c != 0 {
		return c < 0
	} else if c := b1.Compare(b2); c != 0 {
		return c < 0
	} else if c := e1.Compare(e2); c != 0 {
		return c < 0
	} else if c := p1.Compare(p2); c != 0 {
		return c > 0
	}
	return t1.Id < t2.Id
}

func FilterByRid(rid int) FilterFunc { return func(t *Tache) bool { return t.Rid == rid } }

func FilterById(id int) FilterFunc { return func(t *Tache) bool { return t.Id == id } }

func FilterNot(f FilterFunc) FilterFunc { return func(t *Tache) bool { return !f(t) } }

func MultipleFilter(args ...FilterFunc) FilterFunc {
	return func(t *Tache) bool {
		for _, f := range args {
			if !f(t) {
				return false
			}
		}
		return true
	}
}
