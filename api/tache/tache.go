package tache

import (
	"fmt"

	. "framagit.org/benjamin.vaudour/dctime"
)

type Tache struct {
	Id          int
	Rid         int
	Category    string
	Priority    Priority
	Date        Date
	Begin       Clock
	End         Clock
	Recurrence  Duration
	Description string
	Deleted     bool
}

func (t *Tache) String() string {
	return fmt.Sprintf(`Tache{
  Id:          %v,
  Rid:         %v,
  Category:    %v,
  Priority:    %v,
  Date:        %v,
  Begin:       %v,
  End:         %v,
  Recurrence:  %v,
  Description: %v,
  Deleted:     %v,
}`, t.Id, t.Rid, t.Category, t.Priority, t.Date, t.Begin, t.End, t.Recurrence, t.Description, t.Deleted)
}

func New() *Tache {
	t := new(Tache)
	t.Priority = NoPriority
	t.Date = DateNil
	t.Begin = ClockNil
	t.End = ClockNil
	t.Recurrence = DurationNil
	return t
}

func (t *Tache) Clone() *Tache {
	tc := new(Tache)
	*tc = *t
	return tc
}

func (dest *Tache) CopyFrom(src *Tache) {
	dest.Category = src.Category
	dest.Priority = src.Priority
	dest.Date = src.Date
	dest.Begin = src.Begin
	dest.End = src.End
	dest.Recurrence = src.Recurrence
	dest.Description = src.Description
}

func (t *Tache) Format() *Tache {
	if t.Priority.IsNil() {
		t.Priority = NoPriority
	}
	if t.Date.IsNil() {
		t.Date = DateNil
	}
	if t.Begin.IsNil() {
		t.Begin = ClockNil
	}
	if t.End.IsNil() {
		t.End = ClockNil
	}
	if t.Recurrence.IsNil() {
		t.Recurrence = DurationNil
	}
	if t.Date == DateNil || !t.Recurrence.IsDate() || t.Recurrence.Value() <= 0 {
		t.Recurrence = DurationNil
	}
	if t.Recurrence == DurationNil {
		t.Rid = 0
	}
	if t.End < t.Begin {
		t.End = ClockNil
	}
	return t
}

func (t *Tache) Clock() (b, e Clock) {
	b, e = t.Begin, t.End
	if b == ClockNil {
		b, e = ClockBegin, ClockEnd
	} else if e == ClockNil {
		e = b
	}
	return
}

func (t *Tache) IsForever() bool { return t.Date == DateNil }

func (t *Tache) IsRecurrent() bool { return t.Recurrence != DurationNil }

func (t *Tache) IsValid() bool {
	return len(t.Category) > 0 && len(t.Description) > 0 && (!t.IsRecurrent() || !t.IsForever())
}

func (t *Tache) IsDateBefore(d Date) bool { return !t.IsForever() && t.Date < d }

func (t *Tache) IsDateAfter(d Date) bool { return !t.IsForever() && t.Date > d }

func (t *Tache) IsBeginBefore(d Date, c Clock) bool {
	if t.IsDateBefore(d) {
		return true
	} else if t.IsDateAfter(d) {
		return false
	}
	b, _ := t.Clock()
	return b < c
}

func (t *Tache) IsBeginAfter(d Date, c Clock) bool {
	if t.IsDateBefore(d) {
		return false
	} else if t.IsDateAfter(d) {
		return true
	}
	b, _ := t.Clock()
	return b > c
}

func (t *Tache) IsEndBefore(d Date, c Clock) bool {
	if t.IsDateBefore(d) {
		return true
	} else if t.IsDateAfter(d) {
		return false
	}
	_, e := t.Clock()
	return e < c
}

func (t *Tache) IsEndAfter(d Date, c Clock) bool {
	if t.IsDateBefore(d) {
		return false
	} else if t.IsDateAfter(d) {
		return true
	}
	_, e := t.Clock()
	return e > c
}

func (t *Tache) IsInProgress(d Date, c Clock) bool {
	b, e := t.Clock()
	return t.Date == d && b <= c && e >= c
}

func (t *Tache) IsToday() bool { return t.Date == DateNow() }

func (t *Tache) IsNow() bool {
	d, c := Now()
	return t.IsInProgress(d, c)
}

func (t *Tache) IsPast() bool {
	d, c := Now()
	return t.IsEndBefore(d, c)
}

func (t *Tache) IsFuture() bool {
	d, c := Now()
	return t.IsBeginAfter(d, c)
}

func (t *Tache) IsObsolete() bool { return t.Deleted && (t.IsForever() || t.Date < DateNow()) }

func (t *Tache) IncDate(d Duration) bool {
	v := t.Date
	if t.IsForever() {
		v = DateNow()
	}
	if e, ok := v.Add(d); ok {
		t.Date = e
		return true
	}
	return false
}

func (t *Tache) DecDate(d Duration) bool { return t.IncDate(d.Inv()) }

func (t *Tache) IncBegin(d Duration) bool {
	v := t.Begin
	if v == ClockNil {
		v = ClockNow()
	}
	if e, ok := v.Add(d); ok {
		t.Begin = e
		return true
	}
	return false
}

func (t *Tache) DecBegin(d Duration) bool { return t.IncBegin(d.Inv()) }

func (t *Tache) IncEnd(d Duration) bool {
	v := t.End
	if v == ClockNil {
		v = t.Begin
	}
	if e, ok := v.Add(d); ok {
		t.End = e
		return true
	}
	return false
}

func (t *Tache) DecEnd(d Duration) bool { return t.IncEnd(d.Inv()) }
