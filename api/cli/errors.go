package cli

const (
	enotice = iota
	einformation
	ewarning
	eerror
	ecritical
)

const (
	readError         = "Impossible d’initialiser la BDD: %v"
	writeError        = "Impossible de sauvegarder la BDD: %v"
	dbNotOpenedError  = "La BDD n’est pas ouverte"
	countNotice       = "%d tâche(s) %s(s)"
	actionNotice      = "Tâche %d '%s' %s"
	deleteAllQuestion = "Voulez-vous supprimer toutes les tâches associées à la tâche %d '%s' ?"
	modifyAllQuestion = "Voulez-vous modifier toutes les tâches associées à la tâche %d '%s' ?"
	actionError       = "Impossible d’exécuter l’action '%s': %s"
	invalidAction     = "Action invalide"
	invalidOption     = "Option '%s' invalide"
	invalidArg        = "Argument '%s' invalide pour l’option '%s'"
	idNeeded          = "Au moins un ID de tâche est requis"
	idNotParsed       = "IDs '%s' invalides"
	optionsNeeded     = "Des options sont requises"
	optionNeedArg     = "L’option '%s' nécessite un argument"
)
