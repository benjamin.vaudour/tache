package cli

import (
	"framagit.org/benjamin.vaudour/shell/console"
	"framagit.org/benjamin.vaudour/strutil/align"
	"framagit.org/benjamin.vaudour/tache/api/files"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

type option struct {
	name   string
	help   string
	arg    string
	valid  func(string) (interface{}, bool)
	filter func(interface{}) tache.FilterFunc
	exec   func(*tache.Tache, interface{})
}

type arguments struct {
	ids     []int
	options []*option
	args    []interface{}
}

type action struct {
	name     string
	help     string
	dneeded  bool
	ineeded  bool
	ifac     bool
	ofac     bool
	option   map[string]*option
	notshell bool
	notstat  bool
}

type field struct {
	name  string
	size  int
	align align.AlignmentType
	str   func(*tache.Tache) string
}

type view struct {
	vfields      []*field
	sfields      []*field
	sep          string
	prompt       string
	noColor      bool
	headerStyle  []string
	counterStyle []string
	bgStyle      []string
	taskStyle    map[string][]string
}

type App struct {
	name        string
	interactive bool
	terminal    console.Terminal
	files       *files.Files
	view        *view
}
