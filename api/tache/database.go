package tache

import (
	"framagit.org/benjamin.vaudour/collection/v2"
	. "framagit.org/benjamin.vaudour/dctime"
)

type Database struct {
	recurrences List
	tasks       List
}

func NewDatabase() *Database { return &Database{} }

func nid(db *Database) (id int) {
	id = 1
	for _, t := range db.tasks {
		if id <= t.Id {
			id = t.Id + 1
		}
	}
	return
}
func nrid(db *Database) (rid int) {
	rid = 1
	for db.recurrences.ContainsRid(rid) {
		rid++
	}
	return
}

func (db *Database) rpush(t *Tache) (ok bool) {
	if ok = t.IsValid() && t.Rid > 0 && !db.recurrences.ContainsRid(t.Rid); ok {
		db.recurrences.Append(t)
	}
	return
}
func (db *Database) tpush(t *Tache) (ok bool) {
	if ok = t.IsValid() && t.Id > 0 && !db.tasks.ContainsId(t.Id); ok {
		if ok = !t.IsRecurrent() || db.recurrences.ContainsRid(t.Rid); ok {
			db.tasks.Append(t)
		}
	}
	return
}

func (db *Database) rpop(rid int) (tsks List, ok bool) {
	f := FilterByRid(rid)
	ridx := db.recurrences.IndexOf(f)
	if ok = ridx >= 0; ok {
		tsks, db.tasks = db.tasks.Split(f)
		db.recurrences.Remove(ridx)
	}
	return
}
func (db *Database) tpop(id int) (t *Tache, ok bool) {
	var tsks List
	tsks, db.tasks = db.tasks.Split(FilterById(id))
	if ok = len(tsks) > 0; ok {
		t = tsks[0]
	}
	return
}

func (db *Database) RemoveObsolete() {
	for _, t := range db.recurrences {
		if t.Deleted {
			db.rpop(t.Rid)
		}
	}
	db.tasks = db.tasks.Filter(func(t *Tache) bool { return !t.IsObsolete() })
}

func (db *Database) AddMissingTasks(rid, count int) (tsks List) {
	tr, pass := db.recurrences.Find(FilterByRid(rid))
	if !pass {
		return
	}
	dc, dr, r := DateNow(), tr.Date, tr.Recurrence
	dl := dr
	for dr < dc {
		dr, _ = dr.Add(r)
	}
	tr.Date = dr
	c := 0
	for _, t := range db.tasks.Filter(FilterByRid(rid)) {
		for dl <= t.Date {
			dl, _ = dl.Add(r)
		}
		if !t.Deleted && t.Date >= dc {
			c++
		}
	}
	id := nid(db)
	for c < count {
		t := tr.Clone()
		t.Id, t.Date = id, dl
		if db.tpush(t) {
			tsks.Append(t)
			if dl >= dc {
				c++
			}
		}
		id++
		dl, _ = dl.Add(r)
	}
	return
}

func (db *Database) Resequence(f LessFunc) {
	db.tasks = db.tasks.Sort(f)
	for i, t := range db.tasks {
		t.Id = i + 1
	}
}

func (db *Database) Clean(count int, f LessFunc) {
	db.RemoveObsolete()
	for _, t := range db.recurrences {
		db.AddMissingTasks(t.Rid, count)
	}
	db.Resequence(f)
}

func (db *Database) Find(ids ...int) List {
	m := collection.SliceToSet(ids)
	f := func(t *Tache) bool { return m.Contains(t.Id) }
	return db.tasks.Filter(f).Clone()
}

func (db *Database) FindAll() List { return db.tasks.Clone().Sort(LessById) }

func (db *Database) Search(filt ...FilterFunc) List {
	return db.tasks.Filter(MultipleFilter(filt...)).Clone()
}

func (db *Database) Close(id int) (tc *Tache, ok bool) {
	if t, exist := db.tasks.Find(FilterById(id)); exist && !t.Deleted {
		tc, ok = t.Clone(), true
		t.Deleted = true
	}
	return
}

func (db *Database) Open(id int) (tc *Tache, ok bool) {
	if t, exist := db.tasks.Find(FilterById(id)); exist && t.Deleted {
		tc, ok = t.Clone(), true
		t.Deleted = false
	}
	return
}

func (db *Database) Del(id int, recursively bool) (tsks List, ok bool) {
	if t, exist := db.tasks.Find(FilterById(id)); exist {
		if !t.IsRecurrent() {
			if t, ok = db.tpop(id); ok {
				tsks.Append(t)
			}
		} else if !recursively {
			if !t.Deleted {
				tsks.Append(t.Clone())
				ok, t.Deleted = true, true
			}
		} else {
			tsks, ok = db.rpop(t.Rid)
		}
	}
	return
}

func (db *Database) Add(t *Tache) bool {
	t.Format()
	if t.IsRecurrent() {
		t.Id, t.Rid = 0, nrid(db)
		return db.rpush(t)
	} else {
		t.Id, t.Rid = nid(db), 0
		return db.tpush(t)
	}
	return false
}

func (db *Database) Modify(tm *Tache, recursively bool) (deleted List, added, modified *Tache, ok bool) {
	tm = tm.Clone().Format()
	t, exist := db.tasks.Find(FilterById(tm.Id))
	if !exist || !tm.IsValid() || tm.Deleted {
		return
	}
	modified = t.Clone()
	if !t.IsRecurrent() {
		if !tm.IsRecurrent() {
			t.CopyFrom(tm)
			ok = true
		} else if ok = db.Add(tm); ok {
			added = tm
			db.tpop(t.Id)
			deleted.Append(t)
		}
	} else if !recursively {
		if ok = db.Add(tm); ok {
			added = tm
			deleted.Append(t.Clone())
			t.Deleted = true
		}
	} else if deleted, ok = db.rpop(t.Rid); ok {
		if ok = db.Add(tm); ok {
			added = tm
		}
	}
	return
}

func (db *Database) GetTasks() (tasks, recurrences List) {
	return db.tasks.Clone(), db.recurrences.Clone()
}

func (db *Database) SetTasks(tasks, recurrences List) {
	db.tasks, db.recurrences = tasks.Clone(), recurrences.Clone()
}
