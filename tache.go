package main

import (
	"os"
	"path"

	"framagit.org/benjamin.vaudour/conf"
	"framagit.org/benjamin.vaudour/shell/console/readline"
	"framagit.org/benjamin.vaudour/tache/api/cli"
	"framagit.org/benjamin.vaudour/tache/api/files"
	//"github.com/peterh/liner"
)

const appName = "tache"

var confPath string

func init() {
	confDir := os.Getenv("XDG_CONFIG_HOME")
	if confDir == "" {
		confDir = path.Join(os.Getenv("HOME"), ".config")
	}
	confDir = path.Join(confDir, appName)
	confPath = path.Join(confDir, appName+".conf")
	_, err := os.Stat(confPath)
	if err != nil {
		if !os.IsNotExist(err) {
			panic(err)
		}
		if err = os.MkdirAll(confDir, 0755); err != nil {
			panic(err)
		}
		cf := conf.NewFile(files.Template, confPath)
		if err = cf.Save(); err != nil {
			panic(err)
		}
	}
}

func main() {
	//term := liner.NewLiner()
	//defer term.Close()
	//term.SetCtrlCAborts(true)
	//term.SetWordCompleter(cli.WordCompleter)
	term := new(readline.Line)
	app := cli.New(term, confPath)
	app.Execute(os.Args)
}
