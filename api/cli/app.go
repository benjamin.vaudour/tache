package cli

import (
	"strings"

	"framagit.org/benjamin.vaudour/conf"
	"framagit.org/benjamin.vaudour/shell/console"
	"framagit.org/benjamin.vaudour/shell/scanner"
	"framagit.org/benjamin.vaudour/tache/api/files"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

func (app *App) db() *tache.Database {
	return app.files.Database()
}

func (app *App) conf() *conf.Configuration {
	return app.files.Configuration()
}

func New(terminal console.Terminal, configPath string) *App {
	app := &App{
		terminal: terminal,
		files:    files.NewFiles(configPath),
	}
	app.view = newView(app.conf())
	return app
}

func (app *App) exec(args []string) (ret int) {
	arg := new(arguments)
	a, msg := arg.parse(args)
	if msg != nil {
		return app.view.printMessage(msg)
	}
	f := aexec[a.name]
	f(app, arg)
	return
}

func (app *App) loop() (ret int) {
	for app.interactive && ret == 0 {
		s, _ := app.terminal.Prompt(app.view.prompt)
		app.terminal.AppendHistory(s)
		sc := scanner.NewScanner(strings.NewReader(s))
		var args []string
		for sc.Scan() {
			args = append(args, sc.Text())
		}
		if ret = app.exec(args); ret != ecritical && ret != eerror {
			ret = 0
		}
	}
	return
}

func (app *App) Execute(args []string) (ret int) {
	app.name = args[0]
	if ret = app.exec(args[1:]); ret == 0 {
		if app.interactive {
			ret = app.loop()
		}
	}
	if ret == 0 && app.files.IsDbLoaded {
		if err := app.files.SaveDatabase(); err != nil {
			ret = app.view.printMessage(console.NewError(ecritical, err.Error()))
		}
	}
	return
}
