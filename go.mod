module framagit.org/benjamin.vaudour/tache

go 1.18

require (
	framagit.org/benjamin.vaudour/collection/v2 v2.0.0
	framagit.org/benjamin.vaudour/conf v1.0.0
	framagit.org/benjamin.vaudour/dctime v1.0.1
	framagit.org/benjamin.vaudour/shell v1.2.0
	framagit.org/benjamin.vaudour/strutil v1.0.1
)

require (
	framagit.org/benjamin.vaudour/collection v1.0.0 // indirect
	framagit.org/benjamin.vaudour/converter v1.0.2 // indirect
)
