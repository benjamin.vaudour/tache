package database

import (
	"encoding/json"
	"io"

	. "framagit.org/benjamin.vaudour/dctime"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

type jsontask struct {
	Id          int
	Rid         int
	Category    string
	Priority    string
	Date        string
	Begin       string
	End         string
	Recurrence  string
	Description string
	Deleted     bool
}

type jsondb struct {
	Recurrences []jsontask
	Tasks       []jsontask
}

func decode(e jsontask) *tache.Tache {
	t := tache.New()
	t.Id = e.Id
	t.Rid = e.Rid
	t.Category = e.Category
	t.Priority, _ = tache.ParsePriority(e.Priority)
	t.Date, _ = ParseDate(e.Date)
	t.Begin, _ = ParseClock(e.Begin)
	t.End, _ = ParseClock(e.End)
	t.Recurrence, _ = ParseDuration(e.Recurrence)
	t.Description = e.Description
	t.Deleted = e.Deleted
	return t
}

func encode(e *tache.Tache) jsontask {
	return jsontask{
		Id:          e.Id,
		Rid:         e.Rid,
		Category:    e.Category,
		Priority:    e.Priority.String(),
		Date:        e.Date.String(),
		Begin:       e.Begin.String(),
		End:         e.End.String(),
		Recurrence:  e.Recurrence.String(),
		Description: e.Description,
		Deleted:     e.Deleted,
	}
}

func open(r io.Reader) (data jsondb, err error) {
	j := json.NewDecoder(r)
	err = j.Decode(&data)
	return
}
func save(data jsondb, w io.Writer) error {
	j := json.NewEncoder(w)
	j.SetEscapeHTML(false)
	//j.SetIndent("", "\t")
	return j.Encode(data)
}

func EncodeJSON(db *tache.Database, w io.Writer) error {
	tsks, recs := db.GetTasks()
	data := jsondb{
		Recurrences: make([]jsontask, len(recs)),
		Tasks:       make([]jsontask, len(tsks)),
	}
	for i, t := range recs {
		data.Recurrences[i] = encode(t)
	}
	for i, t := range tsks {
		data.Tasks[i] = encode(t)
	}
	return save(data, w)
}

func DecodeJSON(db *tache.Database, r io.Reader) error {
	data, err := open(r)
	if err != nil {
		return err
	}
	recs := make(tache.List, len(data.Recurrences))
	for i, e := range data.Recurrences {
		recs[i] = decode(e)
	}
	tsks := make(tache.List, len(data.Tasks))
	for i, e := range data.Tasks {
		tsks[i] = decode(e)
	}
	db.SetTasks(tsks, recs)
	return nil
}

func NewJSONLoader() Loader {
	return NewLoader(EncodeJSON, DecodeJSON)
}
