package cli

import (
	"fmt"
	"strings"

	"framagit.org/benjamin.vaudour/strutil/align"
	"framagit.org/benjamin.vaudour/strutil/style"
)

func _hlist(t string, args []string) {
	line := fmt.Sprintf("\n%s disponibles : %s", t, strings.Join(args, ", "))
	fmt.Println(style.Format(line, "bold"))
}

func _hfac(s string, fac bool) string {
	if fac {
		s = fmt.Sprintf("[%s]", s)
	}
	return s
}

func _haction(a *action) {
	args := []string{a.name}
	if a.ineeded {
		args = append(args, _hfac("{<id>}", a.ifac))
	}
	if len(a.option) > 0 {
		args = append(args, _hfac("{<option> [<arg>]}", a.ofac))
	}
	fmt.Println(align.TabulateLeft(strings.Join(args, " "), 2))
	fmt.Println(align.TabulateLeft(style.Format(a.help, "italic"), 4))
}

func _hoption(o *option) {
	fmt.Println(align.TabulateLeft(fmt.Sprintf("%s %s", o.name, o.arg), 2))
	fmt.Println(align.TabulateLeft(style.Format(o.help, "italic"), 4))
}

func helpUsage(appname string) {
	fmt.Print(style.Format("Usage : ", "underline", "bold"))
	fmt.Println(style.Format(appname+" <action> [{<id>}] [{<option> [<arg>]}]", "bold"))
}

func helpActions(interactive bool) {
	_hlist("Actions", actions)
	for _, e := range actions {
		a := maction[e]
		if (interactive && a.notshell) || (!interactive && a.notstat) {
			continue
		}
		_haction(a)
	}
}

func helpOptions() {
	_hlist("Options", options)
	for _, e := range options {
		o := soption[e]
		_hoption(o)
	}
}
