package cli

import (
	"strings"

	. "framagit.org/benjamin.vaudour/dctime"
	"framagit.org/benjamin.vaudour/tache/api/tache"
)

const (
	ocat  = "c"
	opri  = "p"
	orpri = "--p"
	odat  = "d"
	oidat = "+d"
	oddat = "-d"
	ordat = "--d"
	obeg  = "b"
	oibeg = "+b"
	odbeg = "-b"
	orbeg = "--b"
	oend  = "e"
	oiend = "+e"
	odend = "-e"
	orend = "--e"
	orec  = "r"
	orrec = "--r"
	oname = "n"
	odel  = "del"
	onot  = "not"
)

func vstr(s string) (interface{}, bool) { return s, len(s) > 0 }
func vpri(s string) (interface{}, bool) { return tache.ParsePriority(s) }
func vdat(s string) (interface{}, bool) { return ParseDate(s) }
func vclo(s string) (interface{}, bool) { return ParseClock(s) }
func vrec(s string) (interface{}, bool) {
	v, ok := ParseDuration(s)
	if ok = ok && (v == DurationNil || (v.IsDate() && v.Value() > 0)); !ok {
		v = DurationNil
	}
	return v, ok
}
func vddat(s string) (interface{}, bool) {
	v, ok := ParseDuration(s)
	if ok = ok && v.IsDate() && v.Value() >= 0; !ok {
		v = DurationNil
	}
	return v, ok
}
func vdclo(s string) (interface{}, bool) {
	v, ok := ParseDuration(s)
	if ok = ok && v.IsClock() && v.Value() >= 0; !ok {
		v = DurationNil
	}
	return v, ok
}

func fstr(v string, e interface{}) bool {
	return strings.Contains(strings.ToLower(v), strings.ToLower(e.(string)))
}
func fpri(v tache.Priority, e interface{}) bool { return v == e.(tache.Priority) }
func fdat(v Date, e interface{}) bool           { return v == e.(Date) }
func fclo(v Clock, e interface{}) bool          { return v == e.(Clock) }
func frec(v Duration, e interface{}) bool       { return v == e.(Duration) }

func inow(e interface{}) (Date, Clock) { return NowIn(e.(Duration)) }
func idnow(e interface{}) Date         { return DateNowIn(e.(Duration)) }

var options = []string{
	ocat,
	opri,
	orpri,
	odat,
	oidat,
	oddat,
	ordat,
	obeg,
	oibeg,
	odbeg,
	orbeg,
	oend,
	oiend,
	odend,
	orend,
	orec,
	orrec,
	oname,
	odel,
	onot,
}

var moption = map[string]*option{
	ocat: &option{
		name:   ocat,
		help:   "Catégorie",
		arg:    "string",
		valid:  func(e string) (interface{}, bool) { return vstr(e) },
		filter: func(e interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return fstr(t.Category, e) } },
		exec:   func(t *tache.Tache, e interface{}) { t.Category = e.(string) },
	},
	opri: &option{
		name:   opri,
		help:   "Priorité",
		arg:    "(H|M|L)",
		valid:  func(e string) (interface{}, bool) { return vpri(e) },
		filter: func(e interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return fpri(t.Priority, e) } },
		exec:   func(t *tache.Tache, e interface{}) { t.Priority = e.(tache.Priority) },
	},
	orpri: &option{
		name: orpri,
		help: "Priorité (suppression)",
		filter: func(interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { return t.Priority == tache.NoPriority }
		},
		exec: func(t *tache.Tache, e interface{}) { t.Priority = tache.NoPriority },
	},
	odat: &option{
		name:   odat,
		help:   "Date",
		arg:    "(yyymmdd|yyyy.mm.dd|dd.mm.yyyy)",
		valid:  func(e string) (interface{}, bool) { return vdat(e) },
		filter: func(e interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return fdat(t.Date, e) } },
		exec:   func(t *tache.Tache, e interface{}) { t.Date = e.(Date) },
	},
	oidat: &option{
		name:  oidat,
		help:  "Date (incrémentation)",
		arg:   "<n>(y|m|w|d)",
		valid: func(e string) (interface{}, bool) { return vddat(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { return t.IsDateAfter(idnow(e)) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.IncDate(e.(Duration)) },
	},
	oddat: &option{
		name:  oddat,
		help:  "Date (décrémentation)",
		arg:   "<n>(y|m|w|d)",
		valid: func(e string) (interface{}, bool) { return vddat(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { return t.IsDateBefore(idnow(e)) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.DecDate(e.(Duration)) },
	},
	ordat: &option{
		name:   ordat,
		help:   "Date (suppression)",
		filter: func(interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return t.IsForever() } },
		exec:   func(t *tache.Tache, e interface{}) { t.Date = DateNil },
	},
	obeg: &option{
		name:   obeg,
		help:   "Début",
		arg:    "(hhmm|hh:mm)",
		valid:  func(e string) (interface{}, bool) { return vclo(e) },
		filter: func(e interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return fclo(t.Begin, e) } },
		exec:   func(t *tache.Tache, e interface{}) { t.Begin = e.(Clock) },
	},
	oibeg: &option{
		name:  oibeg,
		help:  "Début (incrémentation)",
		arg:   "<n>(H|M)",
		valid: func(e string) (interface{}, bool) { return vdclo(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { d, c := inow(e); return t.IsBeginAfter(d, c) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.IncBegin(e.(Duration)) },
	},
	odbeg: &option{
		name:  odbeg,
		help:  "Début (décrémentation)",
		arg:   "<n>(H|M)",
		valid: func(e string) (interface{}, bool) { return vdclo(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { d, c := inow(e); return t.IsBeginBefore(d, c) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.DecBegin(e.(Duration)) },
	},
	orbeg: &option{
		name:   orbeg,
		help:   "Début (suppression)",
		filter: func(interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return t.Begin == ClockNil } },
		exec:   func(t *tache.Tache, e interface{}) { t.Begin = ClockNil },
	},
	oend: &option{
		name:   oend,
		help:   "Fin",
		arg:    "(hhmm|hh:mm)",
		valid:  func(e string) (interface{}, bool) { return vclo(e) },
		filter: func(e interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return fclo(t.End, e) } },
		exec:   func(t *tache.Tache, e interface{}) { t.End = e.(Clock) },
	},
	oiend: &option{
		name:  oiend,
		help:  "Fin (incrémentation)",
		arg:   "<n>(H|M)",
		valid: func(e string) (interface{}, bool) { return vdclo(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { d, c := inow(e); return t.IsEndAfter(d, c) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.IncEnd(e.(Duration)) },
	},
	odend: &option{
		name:  odend,
		help:  "Fin (décrémentation)",
		arg:   "<n>(H|M)",
		valid: func(e string) (interface{}, bool) { return vdclo(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { d, c := inow(e); return t.IsEndBefore(d, c) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.DecEnd(e.(Duration)) },
	},
	orend: &option{
		name:   orend,
		help:   "Fin (suppression)",
		filter: func(interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return t.End == ClockNil } },
		exec:   func(t *tache.Tache, e interface{}) { t.End = ClockNil },
	},
	orec: &option{
		name:  orec,
		help:  "Récurrence",
		arg:   "<n>(y|m|w|d)",
		valid: func(e string) (interface{}, bool) { return vrec(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { return frec(t.Recurrence, e) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.Recurrence = e.(Duration) },
	},
	orrec: &option{
		name:   orrec,
		help:   "Récurrence (suppression)",
		filter: func(interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return !t.IsRecurrent() } },
		exec:   func(t *tache.Tache, e interface{}) { t.Recurrence = DurationNil },
	},
	oname: &option{
		name:  oname,
		help:  "Description",
		arg:   "string",
		valid: func(e string) (interface{}, bool) { return vstr(e) },
		filter: func(e interface{}) tache.FilterFunc {
			return func(t *tache.Tache) bool { return fstr(t.Description, e) }
		},
		exec: func(t *tache.Tache, e interface{}) { t.Description = e.(string) },
	},
}

var soption = (func() map[string]*option {
	m := make(map[string]*option)
	for i, o := range moption {
		m[i] = o
	}
	m[odel] = &option{
		name:   odel,
		help:   "Tâche(s) supprimée(s) (recherche uniquement)",
		filter: func(interface{}) tache.FilterFunc { return func(t *tache.Tache) bool { return t.Deleted } },
	}
	m[onot] = &option{
		name: onot,
		help: "Inverse le critère qui suit (recherche uniquement)",
	}
	return m
})()
